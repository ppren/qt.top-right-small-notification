# qt.top-right-small-notification


- 初步的设计蓝图

    ![](assets/images/d67680ff1cdd43f084534be8c27bea4e.png)


- 最终效果

    - 被改进的效果(未使用)

        ```css
        /* 目前在 statuswidget.ui 的 widget 控件中被注释
           使用的是原始的颜色效果 */
        ```

        ![](assets/images/20221128120151.png)  

    - 原本的颜色效果

        ```css
        /* 在 statuswidget.ui 的 widget 控件中使用以下效果  */
        background-color: #006B83;
        color: rgb(255, 255, 255);
        ```
        
        ![](assets/images/20221128120356.png)  

    - 效果预览
        
        ![](assets/images/20221128121407.png)  

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>
#include <QTimer>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
  , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    StatusWidget *status = (StatusWidget*)ui->widget;
    status->setShowMessage("提示","欢迎使用本程序");
    // 默认上进右出
    status->setDirection(StatusWidget::TopIn, StatusWidget::RightOut); 
    status->hide(); // 默认不显示这个控

    QTimer *timer = new QTimer;
    timer->setInterval(500);
    connect(timer, &QTimer::timeout, this, [=](){
        status->show();
        status->start();
        timer->stop();
    });
    timer->start();
}
MainWindow::~MainWindow()
{
	
}

void MainWindow::on_pushButton_clicked()
{
    StatusWidget *status = (StatusWidget*)ui->widget;
    status->setShowMessage("提示","这是上进右出");
    status->setDirection(StatusWidget::TopIn, StatusWidget::RightOut);
    status->start();
}

void MainWindow::on_pushButton_2_clicked()
{
    ui->widget->setShowMessage("提示", "这是右进上出");
    ui->widget->setDirection(StatusWidget::RightIn, StatusWidget::TopOut);
    ui->widget->start();
}

void MainWindow::on_pushButton_3_clicked()
{
    ui->widget->setDirection(StatusWidget::BottomIn, StatusWidget::RightOut);
    ui->widget->start();
}

void MainWindow::on_pushButton_4_clicked()
{
    ui->widget->setDirection(StatusWidget::RightIn, StatusWidget::BottomOut);
    ui->widget->start();
}
